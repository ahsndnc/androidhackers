#/usr/system/python
import subprocess
import os
import time
import shutil
import json
import datetime

x=0

def init_diction(dict_type):
	diction = {}
	if dict_type == 'battery':
		diction['battery_status']      = []
		diction['battery_health']      = []
		diction['battery_level']       = []
		diction['battery_temperature'] = []
		return diction
	if dict_type == 'cpu':
		diction['cpu_total']      = []
		diction['cpu_user']       = []
		diction['cpu_kernel']     = []
		diction['cpu_load1']      = []
		diction['cpu_load2']      = []
		diction['cpu_load3']      = []
		diction['cpu_hog1']       = []
		diction['cpu_hog2']       = []
		diction['cpu_hog3']       = []
		diction['cpu_hog4']       = []
		diction['cpu_hog5']       = []
		return diction
	if dict_type == 'ram':
		diction['ram_total']      = []
		diction['ram_free']       = []
		diction['ram_used']       = []
		return diction

	return diction

def battery_value_map(label, value):
	if label == 'battery_health':
		try:
			value = int(value)
			if value == 2:
				return 'Good'
			if value == 3:
				return 'Overheat'
			if value == 4:
				return 'Dead'
			if value == 5:
				return 'Over voltage'
			if value == 6:
				return 'Unspecified failure'
			if value == 7:
				return 'Cold'
		except:
			return 'Unknown'

	if label == 'battery_status':
		try:
			value = int(value)
			if value == 2:
				return 'Charging'
			if value == 3:
				return 'Discharging'
			if value == 4:
				return 'Not Charging'
			if value == 5:
				return 'Full'
			else:
				return 'Unknown'
		except:
			return 'Unknown'

battery_dict = init_diction('battery')
cpu_dict     = init_diction('cpu')
ram_dict     = init_diction('ram')


while(True):
	print(x)
	x += 1

	record_time = datetime.datetime.now().isoformat()

	##################### Put Battery Info #########################
	battery_output = subprocess.check_output(['dumpsys', 'battery'])
	values_added = []


	for info in battery_output.split('\n'):
		data = info.split(':')
		if (len(data) != 2):
			continue
		label = ''.join(['battery_', data[0].strip()])
		if label in battery_dict.keys() and label not in values_added:
			values_added.append(label)
			local_dict = {}
			local_dict['time'] = record_time
			local_dict['value'] = data[1].strip()
			if label == 'battery_health' or label == 'battery_status':
				local_dict['value'] = battery_value_map(label, data[1].strip())

			battery_dict[label].append(local_dict)

	##################### MAKE CPU DICTIONARIES #####################
	try:
		cpu_output = subprocess \
			.check_output(['dumpsys', 'cpuinfo']) \
			.strip().split('\n')

		total_cpu = cpu_output[-1]
		total_cpu_percent_str = total_cpu.split(':')[0].strip()
		cpu_makeup = total_cpu.split(':')[1].split('+')

		cpu_percent = total_cpu_percent_str.split(b'%')[0].strip()
		total_str = total_cpu_percent_str.split(b'%')[1].strip()

		user=cpu_makeup[0].split(b'%')[0].strip()
		user_str=cpu_makeup[0].split(b'%')[1].strip()

		kernel=cpu_makeup[1].split(b'%')[0].strip()
		kernel_str=cpu_makeup[1].split(b'%')[1].strip()

		# Add cpu_total
		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = cpu_percent
		cpu_dict['cpu_total'].append(local_dict)

		# Add cpu_user
		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = user
		cpu_dict['cpu_user'].append(local_dict)

		'''
		# Add cpu_kernel
		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = user
		cpu_dict['cpu_kernel'].append(local_dict)
		'''

		load_str  = cpu_output[0]
		load_key  = load_str.split(b':')[0].strip()
		load_vals = load_str.split(b':')[1].strip()
		load_val1 = load_vals.split(b'/')[0].strip()
		load_val2 = load_vals.split(b'/')[1].strip()
		load_val3 = load_vals.split(b'/')[2].strip()

		# Add cpu_load1
		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = load_val1
		cpu_dict['cpu_load1'].append(local_dict)

		# Add cpu_load2
		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = load_val2
		cpu_dict['cpu_load2'].append(local_dict)

		# Add cpu_load3
		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = load_val3
		cpu_dict['cpu_load3'].append(local_dict)

	except Exception as e:
		print('cpu error')
		print(e)
		continue
		# idk don't do anything ..?

	######### CPU GET THE APPS THAT HOG THE MOST CPU ###########
	hog1 = cpu_output[2].strip().split(' ')
	hog1_percent = hog1[0].split('%')[0].strip()
	hog1_app = hog1[1].split('/')[1].strip()[:-1]
	local_dict = {}
	local_dict['time'] = record_time
	local_dict['name'] = hog1_app
	local_dict['value'] = (hog1_percent.split('.')[0])
	cpu_dict['cpu_hog1'].append(local_dict)	

	hog2 = cpu_output[3].strip().split(' ')
	hog2_percent = hog2[0].split('%')[0].strip()
	hog2_app = hog2[1].split('/')[1].strip()[:-1]
	local_dict = {}
	local_dict['time'] = record_time
	local_dict['name'] = hog2_app
	local_dict['value'] = (hog2_percent.split('.')[0])
	cpu_dict['cpu_hog2'].append(local_dict)	

	hog3 = cpu_output[4].strip().split(' ')
	hog3_percent = hog3[0].split('%')[0].strip()
	hog3_app = hog3[1].split('/')[1].strip()[:-1]
	local_dict = {}
	local_dict['time'] = record_time
	local_dict['name'] = hog3_app
	local_dict['value'] = (hog3_percent.split('.')[0])
	cpu_dict['cpu_hog3'].append(local_dict)	

	hog4 = cpu_output[5].strip().split(' ')
	hog4_percent = hog4[0].split('%')[0].strip()
	hog4_app = hog4[1].split('/')[1].strip()[:-1]
	local_dict = {}
	local_dict['time'] = record_time
	local_dict['name'] = hog4_app
	local_dict['value'] = (hog4_percent.split('.')[0])
	cpu_dict['cpu_hog4'].append(local_dict)

	hog5 = cpu_output[6].strip().split(' ')
	hog5_percent = hog5[0].split('%')[0].strip()
	hog5_app = hog5[1].split('/')[1].strip()[:-1]
	local_dict = {}
	local_dict['time'] = record_time
	local_dict['name'] = hog5_app
	local_dict['value'] = (hog5_percent.split('.')[0])
	cpu_dict['cpu_hog5'].append(local_dict)


	##################### MEMINFO DATA GETTING AND DICTS #####################
	try:
		meminfo_output = subprocess.check_output(['dumpsys', 'meminfo']).strip().split('\n')
		mem_status_str = meminfo_output[len(meminfo_output)-5]
		total_RAM = mem_status_str.split(' ')[2].strip()
		mem_status = mem_status_str.split(' ')[5].strip()
		mem_status = mem_status[:-1]

		free_ram_str = meminfo_output[len(meminfo_output) - 4].strip()
		free_ram_arr = free_ram_str.split(' ')
		ram_free = free_ram_arr[2].strip() #kB

		ram_cached_pss = free_ram_arr[4].strip()
		ram_cached_pss = ram_cached_pss[1:]

		ram_cached_kernel = free_ram_arr[8].strip()

		used_ram_str = meminfo_output[len(meminfo_output)-3].strip()
		used_ram_arr = used_ram_str.split(' ')

		total_used_ram = used_ram_arr[2]
		used_ram_pss = used_ram_arr[4][1:]
		used_ram_kernel = used_ram_arr[8]
		lost_ram_str = meminfo_output[len(meminfo_output)-2].strip()
		lost_ram = lost_ram_str.split(' ')[2].strip()

		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = total_RAM
		ram_dict['ram_total'].append(local_dict)


		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = ram_free
		ram_dict['ram_free'].append(local_dict)

		local_dict = {}
		local_dict['time'] = record_time
		local_dict['value'] = total_used_ram
		ram_dict['ram_used'].append(local_dict)

	except Exception as e:
		print('meminfo error')
		print(e)

	if( x >= 20 and not os.path.isfile('phone_stats.txt')):


		with open('phone_stats.txt', 'w') as f:
			f.write('battery_level\n'       + json.dumps(battery_dict['battery_level']) +'\n')
			f.write('battery_health\n'      + json.dumps(battery_dict['battery_health'])+'\n')
			f.write('battery_status\n'      + json.dumps(battery_dict['battery_status'])+'\n')
			f.write('battery_temperature\n' + json.dumps(battery_dict['battery_temperature'])+'\n')
			f.write('cpu_total\n'           + json.dumps(cpu_dict['cpu_total'])+'\n')
			f.write('cpu_load1\n'           + json.dumps(cpu_dict['cpu_load1'])+'\n')
			f.write('cpu_load2\n'           + json.dumps(cpu_dict['cpu_load2'])+'\n')
			f.write('cpu_load3\n'           + json.dumps(cpu_dict['cpu_load3'])+'\n')
			f.write('cpu_hog1\n'            + json.dumps(cpu_dict['cpu_hog1'])+'\n')
			f.write('cpu_hog2\n'            + json.dumps(cpu_dict['cpu_hog2'])+'\n')
			f.write('cpu_hog3\n'            + json.dumps(cpu_dict['cpu_hog3'])+'\n')
			f.write('cpu_hog4\n'            + json.dumps(cpu_dict['cpu_hog4'])+'\n')
			f.write('cpu_hog5\n'            + json.dumps(cpu_dict['cpu_hog5'])+'\n')
			f.write('ram_total\n'           + json.dumps(ram_dict['ram_total'])+'\n')
			f.write('ram_free\n'            + json.dumps(ram_dict['ram_free'])+'\n')
			f.write('ram_used\n'            + json.dumps(ram_dict['ram_used'])+'\n')
			#battery_temperature
		battery_dict = init_diction('battery')
		cpu_dict     = init_diction('cpu')
		ram_dict     = init_diction('ram')



		x = 0
	
	# Make sure our memory usage isn't too big
	if (x == 60):
		battery_dict = init_diction('battery')
		cpu_dict     = init_diction('cpu')
		ram_dict     = init_diction('ram')

	#sleep for 15 seconds
	time.sleep(15) 




