import os
import subprocess
import time

#/usr/system/python
import subprocess
import os
import time
import shutil
import json
import datetime



# ==========================
def getBatteryLevel():
	battery_output = subprocess.check_output(['dumpsys', 'battery'])
	battery_dict = dict()
	for info in battery_output.split("\n"):
		try:
			invalid_counter = 0;
			if(len(info.split(b':')) > 1):
				battery_key = info.split(b':')[0].strip()
				battery_value = info.split(b':')[1].strip()
				#print(battery_key, battery_value)
				if(battery_key in battery_dict.keys()):
					battery_key = battery_key + '1'
				battery_dict[battery_key] = battery_value
		except IndexError:
			battery_dict[battery_key] = invalid_counter + 1
			invalid_counter = invalid_counter + 1
		except Exception as e:
			print(e)
	battery_level = battery_dict['level']
	return int (battery_level)

# =============================
def getRepeatInterval():
	alarm_output = subprocess.check_output(['dumpsys', 'alarm'])
	alarm_split = alarm_output.split("Batch")
	repeatInterval = -1
	try:
		for batch in alarm_split:
			if "testing.ece" in batch and "RTC" in batch:
				print(batch)
				alarm_info = batch.strip().split(" ")
				for info in alarm_info:
					if "repeatInterval=" in info:
						repeatInterval = int (info.strip().split("=")[1])
	except Exception as e:
			postit('Failed', 'No alarm found, service crashed')
			return None
	return int (repeatInterval)		
#==================



def postit(result, message):
	with open("test_result.txt", "a") as f:
		f.write("[low battery]:" + result + ":" + message + "\n")



def run_battery_test():
	battery_output = subprocess.check_output(['dumpsys', 'battery'])
	first_battery_level = getBatteryLevel()
	time.sleep(2)
	set_low_battery = subprocess.check_output(['dumpsys', 'battery', 'set', 'level', '13'])
	time.sleep(2)
	batteryLevel = getBatteryLevel()
	if(batteryLevel != 13):
		message = ' '.join(["Low battery level was not set correctly, \
			it should be 13%, but was set to", str(batteryLevel)])
		postit('Failed', message)
	time.sleep(60)
	repeatInterval = getRepeatInterval()


	if repeatInterval >= 600000 and repeatInterval <= 800000:
		message = ' '.join(["Debug info is sent every",  str(repeatInterval/1000),  "seconds"])
		postit('Passed', message)
	else:
		message = ' '.join(["Debug info is sent every",  str(repeatInterval/1000),  "seconds", \
			"it should be sent every 600 seconds"])
		postit('Failed', message)


	# set high battery
	set_high_battery = subprocess.check_output(['dumpsys', 'battery', 'set', 'level', '85'])
	time.sleep(2)

	batteryLevel = getBatteryLevel()
	if(batteryLevel != 85):
		message = ' '.join(["High battery level was not set correctly, \
			it should be 85%, but was set to", str(batteryLevel)])
		postit('Failed', message)
	time.sleep(60)
	repeatInterval = getRepeatInterval()

	highRepeatInterval = getRepeatInterval()

	if repeatInterval <= 301000:
		message = ' '.join(["Debug info is sent every",  str(repeatInterval/1000),  "seconds"])
		postit('Passed', message)
	else:
		message = ' '.join(["Debug info is sent every",  str(repeatInterval/1000),  "seconds", \
			"it should be sent every 300 seconds."])
		postit('Failed', message)
	subprocess.check_output(['dumpsys', 'battery', 'reset'])

while True:
        if not os.path.exists('/sdcard/test.run'):
                time.sleep(30)
                continue
        with open('/sdcard/test.run', 'r') as f:
                try:
                        tests = f.read()
                        tests = tests.split('\n')
                        for test in tests:
                                if test == 'battery.py':
                                	print ("Running Battery test")
                                	run_battery_test()

                except:
                        continue
        subprocess.call(['rm', '/sdcard/test.run'])








