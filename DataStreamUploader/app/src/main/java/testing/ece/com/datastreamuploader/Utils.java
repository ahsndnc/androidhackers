package testing.ece.com.datastreamuploader;

import java.util.HashMap;

/**
 * Created by Asad on 4/20/2017.
 */

public class Utils {

    public static String APP_TAG = "DataStreamUploader";
    public static final int SERVICE_ID = 101;
    public static final String FILE_NAME = "phone_stats.txt";
    public static final String TEST_RESULT_FILE = "test_result.txt";
    public static final String UPLOAD_ADDRESS = "https://murmuring-inlet-55023.herokuapp.com/app/postdata/";
    public static final String TEST_ADDRESS = "https://murmuring-inlet-55023.herokuapp.com/app/gettest";
    public static final String UPDATE_TEST = "https://murmuring-inlet-55023.herokuapp.com/app/posttest/";

    public static HashMap<String, String> testMap = new HashMap<>();
    static {
        testMap.put("upload battery test", "battery.py");
    }
}
