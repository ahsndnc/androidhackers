package testing.ece.com.datastreamuploader.Receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.sql.Time;
import java.util.Calendar;

import testing.ece.com.datastreamuploader.DataUploaderService;
import testing.ece.com.datastreamuploader.Utils;

public class ServiceLaunchReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(Utils.APP_TAG, "Received the custom broadcast");

        //Intent startServiceIntent = new Intent(context, DataUploaderService.class);
        //context.startService(startServiceIntent);


        Calendar cal = Calendar.getInstance();

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        long interval = 1000 * 60 * 1; // 1 minute in milliseconds
        Intent serviceIntent = new Intent(context, DataUploaderService.class);
// make sure you **don't** use *PendingIntent.getBroadcast*, it wouldn't work
        PendingIntent servicePendingIntent =
                PendingIntent.getService(context,
                        Utils.SERVICE_ID, // integer constant used to identify the service
                        serviceIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);  // FLAG to avoid creating a second service if there's already one running
// there are other options like setInexactRepeating, check the docs
        am.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,//type of alarm. This one will wake up the device when it goes off, but there are others, check the docs
                cal.getTimeInMillis(),
                interval,
                servicePendingIntent
        );


    }
}
