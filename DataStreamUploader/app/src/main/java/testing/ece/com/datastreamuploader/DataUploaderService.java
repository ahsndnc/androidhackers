package testing.ece.com.datastreamuploader;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.concurrent.Exchanger;


public class DataUploaderService extends Service {

    public String exec(String command) {
        String retour = "";
        try {
            Runtime runtime = Runtime.getRuntime();

            Process p = runtime.exec(command);

            java.io.BufferedReader standardIn = new java.io.BufferedReader(
                    new java.io.InputStreamReader(p.getInputStream()));
            java.io.BufferedReader errorIn = new java.io.BufferedReader(
                    new java.io.InputStreamReader(p.getErrorStream()));
            String line = "";
            while ((line = standardIn.readLine()) != null) {
                retour += line + "\n";
            }
            while ((line = errorIn.readLine()) != null) {
                retour += line + "\n";
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        if (command.contains(".py"))
            try {
                Thread.sleep(30000);
            } catch (Exception e) {
                Log.i(Utils.APP_TAG, e.getMessage());
            }

        return retour;
    }




    String getFileData(String fileName) {

        String result = exec("su root cat /sdcard/" + fileName);
        return result;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(Utils.APP_TAG, "Started the custom service");
        // Check if file exists in directory
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = getApplicationContext().registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;

        if (batteryPct <= 0.2) {
            Calendar cal = Calendar.getInstance();

            AlarmManager am = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            long interval = 1000 * 60 * 10; // 1 minute in milliseconds

            Intent serviceIntent = new Intent(getApplicationContext(), DataUploaderService.class);
// make sure you **don't** use *PendingIntent.getBroadcast*, it wouldn't work
            PendingIntent servicePendingIntent =
                    PendingIntent.getService(getApplicationContext(),
                            Utils.SERVICE_ID, // integer constant used to identify the service
                            serviceIntent,
                            PendingIntent.FLAG_CANCEL_CURRENT);  // FLAG to avoid creating a second service if there's already one running
// there are other options like setInexactRepeating, check the docs
            am.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP,//type of alarm. This one will wake up the device when it goes off, but there are others, check the docs
                    cal.getTimeInMillis(),
                    interval,
                    servicePendingIntent
            );


        }

        new HttpUploadStats().execute();
        new HttpTestRunner().execute();
        return Service.START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private class HttpUploadStats extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            // Get jsonData if file exists
            String jsonString = getFileData(Utils.FILE_NAME);
            if (jsonString == null)
                return null;

            JSONObject jsonObject = new JSONObject();

            String [] jsonInput = jsonString.split("\n");
            for (int i = 0; i < jsonInput.length - 1; i++) {
                if (i % 2 == 0) {
                    try {
                        jsonObject.put(jsonInput[i], jsonInput[i+1]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

                try {

                    URL url = new URL(Utils.UPLOAD_ADDRESS);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("charset", "utf-8");
                    conn.setRequestProperty("Content-Length", Integer.toString(jsonObject.toString().length()));
                    conn.setUseCaches(false);

                    DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
                    printout.write(jsonObject.toString().getBytes("UTF-8"));
                    printout.flush();
                    printout.close();
                    int serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();
                    Log.i(Utils.APP_TAG, serverResponseMessage + ": " + serverResponseCode);

                } catch (Exception e) {
                    Log.d(Utils.APP_TAG, e.getMessage());
                }

                // Delete file
                String result = exec("su root rm /sdcard/phone_stats.txt");

            return null;
        }


    }


    private class HttpTestRunner extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try {

                JSONObject testJson = getJSONObjectFromURL(Utils.TEST_ADDRESS);
                int testID = testJson.getInt("id");
                String testName = testJson.getString("name");
                String scriptName = Utils.testMap.get(testName.toLowerCase());
                String retout = exec("su root rm   /sdcard/test_result.txt");
                retout = exec("su root echo " + scriptName + " >> /sdcard/test.run");

                    try {
                        int count = 0;

                        while (count < 130) {
                            count += 10;
                            Thread.sleep(10 * 1000);
                            String data = getFileData(Utils.TEST_RESULT_FILE).trim();
                            if (data.contains("Passed") || data.contains("Failed"))
                                break;
                        }

                        String [] testOutput = getFileData(Utils.TEST_RESULT_FILE).trim().split(":");
                        String  testResult = testOutput[1];
                        String  testMessage = testOutput.length >= 3 ? testOutput[2] : "";
                        postTestResult(testID, testResult, testMessage);
                        String result = exec("su root rm /sdcard/test.run");
                        result = exec("su root rm /sdcard/test_result.txt");
                    } catch (Exception e) {
                        Log.i(Utils.APP_TAG, e.getMessage());
                    }


                Log.i(Utils.APP_TAG, testJson.toString());

            } catch (Exception e) {
                Log.d(Utils.APP_TAG, e.getMessage());
            }

            return null;
        }


    }

    // TODO: Implement
    void postTestResult(int testID, String testResult, String testMessage) {
        if (!testResult.toLowerCase().contains("failed") && !testResult.toLowerCase().contains("passed"))
            return;
        try {
            JSONObject object = new JSONObject();
            object.put("id", testID);
            object.put("status", testResult);
            object.put("message", testMessage);


            URL url = new URL(Utils.UPDATE_TEST);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(object.toString().length()));
            conn.setUseCaches(false);

            DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
            printout.write(object.toString().getBytes("UTF-8"));
            printout.flush();
            printout.close();
            int serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
            Log.i(Utils.APP_TAG, serverResponseMessage + ": " + serverResponseCode);


        }

        catch( Exception e){
            Log.i(Utils.APP_TAG, e.getMessage());
        }

    }

    public static JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException {

        HttpURLConnection urlConnection = null;
        URL url = new URL(urlString);

        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000);
        urlConnection.setConnectTimeout(15000);
        urlConnection.setDoOutput(true);
        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        String jsonString = sb.toString();

        System.out.println("JSON: " + jsonString);

        return new JSONObject(jsonString);
    }

}
